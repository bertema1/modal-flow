({
    doInit : function(component, event, helper) {
        component.set("v.page", component.get("v.options")[0].title);
    },

    manageF2B : function(component, event, helper) {
        // manage Footer event
        var json = JSON.parse(event.getParam("json"));

        switch (json.action) {
            case 'next':
            case 'back':
                component.set("v.page", json.target);
                helper.confirmAction(json.action);
                break;
            case 'validate':
                // helper.validateAndFire(component, event);
                break;
        }
    },

})
