({
    fireB2Fevent : function (response) {
        var B2F = $A.get("e.c:ModalBody2FooterEvent");
        B2F.setParams({ "json" : response });
        B2F.fire();
    },

    confirmAction : function (target) {
        this.fireB2Fevent(JSON.stringify( { "action": "confirm-action" , "target": target } ));
    }
})
