({
    doInit : function (component, event, helper) {
        helper.init(component);
    },

    closeModal : function (component, event, helper) {
        //closes the modal or popover from the component
        component.find("overlayLib").notifyClose();
    },

    next : function (component, event, helper) {
        helper.modalFlowPage(component, event, 'next');
    },

    back : function (component, event, helper) {
        helper.modalFlowPage(component, event, 'back');
    },

    manageB2F : function(component, event, helper) {
        // manage Body event
        var json = JSON.parse(event.getParam("json"));

        console.log(json);
        switch (json.action) {
            case 'validate':
                component.set("v.isSearchNextValid", json.isValid);
                break;
            case 'confirm-action':
                if (json.target == 'next')
                    helper.next(component, true);
                if (json.target == 'back')
                    helper.back(component, true);
                break;
        }
    }
})